﻿using EmeraldTemplate.MVVM.ViewModel.MainWindow;
using SimpleInjector;
using System;

namespace EmeraldTemplate._composition
{
    public class CompositionRoot : BaseRoot
    {
        [STAThread]
        public static void Main(string[] args)
        {
            new CompositionRoot().Execute<MainWindow, IMainWindowViewModel>();
        }

        public override void CustomInjections(Container container)
        {

        }
    }
}
