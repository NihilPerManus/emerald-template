﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace EmeraldTemplate._composition
{
    public abstract class BaseRoot
    {
        private readonly Container _container = new();
        private App app;

        protected void Execute<TWindow, TContext>() where TWindow : Window where TContext : class
        {
            _container.RegisterSingleton<TWindow>(); //Always register main window.

            CustomInjections(_container);
            AutoWire();

            app = new();
            var window = _container.GetInstance<TWindow>();
            window.DataContext = _container.GetInstance<TContext>();
            app.Run(window);
        }

        /// <summary>
        /// Provides a section for the user to supply custom registrations.
        /// This will override autowiring.
        /// </summary>
        /// <param name="container"></param>
        public abstract void CustomInjections(Container container);


        #region private methods
        /// <summary>
        /// Autowires classes which have been marked with the [AutoWire] attribute.
        /// If an interface has already been registered then it will be ignored
        /// </summary>
        private void AutoWire()
        {
            IEnumerable<Type> alreadyRegistered = _container.GetCurrentRegistrations().Select(x => x.ServiceType);
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            IEnumerable<Type> classes = assemblies
                                .Where(NotSystemClass)
                                .SelectMany(a => a.GetTypes())
                                .Where(t => t.IsClass && !t.IsSealed && !t.IsAssignableTo(typeof(Application)))
                                .Where(t => t.GetCustomAttribute<AutoWireAttribute>() is not null)
                                ;

            IEnumerable<Type> interfaces = assemblies
                                .Where(NotSystemClass)
                                .SelectMany(a => a.GetTypes())
                                .Where(t => t.IsInterface)
                                .Except(alreadyRegistered)
                                ;

            IEnumerable<(Type Class, Type Interface, bool AsSingleton)> joins = classes.Join(interfaces,
                c => $"I{c.Name}",
                i => i.Name,
                (c, i) => (Class: c, Interface: i, c.GetCustomAttribute<AutoWireAttribute>().AsSingleton));

            PrintCollection("Registrations", alreadyRegistered, (x) => x.FullName);
            PrintCollection("Services", interfaces, (x) => x.FullName);
            PrintCollection("CONCRETES", classes, (s) => s.FullName);

            PrintCollection("Joins", joins, (x) => x.Class.FullName);

            joins.ToList()
                .ForEach(j =>
                {
                    if (j.AsSingleton)
                    {
                        _container.RegisterSingleton(j.Interface, j.Class);
                    }
                    else
                    {
                        _container.Register(j.Interface, j.Class);
                    }
                });
        }

        // Ignore the System namespace.
        private bool NotSystemClass(Assembly assembly)
        {
            return assembly.FullName.Split('.').First() != "System";
        }

        //Use for debugging
        private static void PrintCollection<T>(string title, IEnumerable<T> collection, Func<T, string> format)
        {
            Debug.WriteLine($"{title}\n{new string('=', title.Length)}\n{string.Join("\n", collection.Select(x => "  - " + format(x)))}\n");
        }
        #endregion
    }
}
