﻿using EmeraldTemplate._composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmeraldTemplate.MVVM.ViewModel.MainWindow
{
    [AutoWire(AsSingleton: true)]
    public class MainWindowViewModel : IMainWindowViewModel
    {
    }

    public interface IMainWindowViewModel
    {
    }
}
